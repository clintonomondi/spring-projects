package com.edubora.Mpesa;

public class MpesaApiConsume {

	String amount="";
	String phone="";
	String eslip_no="";
	String checkoutrequestid="";
	
	
	public String getCheckoutrequestid() {
		return checkoutrequestid;
	}
	public void setCheckoutrequestid(String checkoutrequestid) {
		this.checkoutrequestid = checkoutrequestid;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getEslip_no() {
		return eslip_no;
	}
	public void setEslip_no(String eslip_no) {
		this.eslip_no = eslip_no;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
}
