package com.edubora.Mpesa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Random;

import org.json.JSONObject;


import com.google.gson.JsonElement;
import com.google.gson.JsonParser;



public class MpesaHelper {
	static Connection conn=null;
	
		
//	public static void logMpesaRequestResponse(String phoneNumber,String amount,String eslip_no,String type,String MerchantRequestID,String 
//			ResponseCode,String CustomerMessage
//			,String CheckoutRequestID,String ResponseDescription,String mpesa_ref,String status) {
//		
//		try {
//			String sql="INSERT INTO mpesa_logs(phone,amount,eslip_no,type,MerchantRequestID,ResponseCode,CustomerMessage,CheckoutRequestID,ResponseDescription,mpesa_ref,status)VALUES(?,?,?,?,?,?,?,?,?,?,?)";
//			 conn=DbManager.getConnection();
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			 prep.setObject(1,phoneNumber);
//			 prep.setObject(2,amount );
//			 prep.setObject(3,eslip_no);
//			 prep.setObject(4,type);
//			 prep.setObject(5,MerchantRequestID);
//			 prep.setObject(6, ResponseCode);
//			 prep.setObject(7,CustomerMessage);
//			 prep.setObject(8, CheckoutRequestID);
//			 prep.setObject(9,ResponseDescription);
//			 prep.setObject(10, mpesa_ref);
//			 prep.setObject(11,status );
//			 prep.execute();
//			 conn.close();
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//	} 
	
//	public static void logPayments(String phoneNumber,String amount,String eslip_no,String type,String MerchantRequestID,String 
//			ResponseCode,String CustomerMessage
//			,String CheckoutRequestID,String ResponseDescription,String mpesa_ref,String status,String ebiller_ref) {
//		
//		try {
//			String sql="INSERT INTO mpesa_payments(phone,amount,eslip_no,type,MerchantRequestID,ResponseCode,CustomerMessage,CheckoutRequestID,ResponseDescription,mpesa_ref,status,ebiller_ref)VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
//			 conn=DbManager.getConnection();
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			 prep.setObject(1,phoneNumber);
//			 prep.setObject(2,amount );
//			 prep.setObject(3,eslip_no);
//			 prep.setObject(4,type);
//			 prep.setObject(5,MerchantRequestID);
//			 prep.setObject(6, ResponseCode);
//			 prep.setObject(7,CustomerMessage);
//			 prep.setObject(8, CheckoutRequestID);
//			 prep.setObject(9,ResponseDescription);
//			 prep.setObject(10, mpesa_ref);
//			 prep.setObject(11,status );
//			 prep.setObject(12,ebiller_ref );
//			 prep.execute();
//			 conn.close();
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//	} 
//	public static void updatePayments(String type,String MerchantRequestID,String CheckoutRequestID,String ResponseDescription,String mpesa_ref,String status) {
//		
//		try {
//			String sql="UPDATE  mpesa_payments SET type=?,ResponseDescription=?,mpesa_ref=?,status=? WHERE MerchantRequestID=? AND CheckoutRequestID=?";
//			 conn=DbManager.getConnection();
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			 prep.setObject(1,type);
//			 prep.setObject(2,ResponseDescription);
//			 prep.setObject(3, mpesa_ref);
//			 prep.setObject(4,status );
//			 prep.setObject(5,MerchantRequestID);
//			 prep.setObject(6, CheckoutRequestID);
//			 prep.execute();
//			 conn.close();
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//	} 
//	
//	
//	public static String getEslipNumber(String MerchantRequestID,String CheckoutRequestID) {
//		String eslip_no="";
//		try {
//			String sql="SELECT eslip_no from mpesa_payments  WHERE MerchantRequestID=? AND CheckoutRequestID=?";
//			 conn=DbManager.getConnection();
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			 prep.setObject(1,MerchantRequestID);
//			 prep.setObject(2, CheckoutRequestID);
//			ResultSet rs=prep.executeQuery();
//			while(rs.next()) {
//				eslip_no=rs.getString("eslip_no");
//			}
//			 conn.close();
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return eslip_no;
//	}
//	public static String getPhone(String MerchantRequestID,String CheckoutRequestID) {
//		String eslip_no="";
//		try {
//			String sql="SELECT phone from mpesa_payments  WHERE MerchantRequestID=? AND CheckoutRequestID=?";
//			 conn=DbManager.getConnection();
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			 prep.setObject(1,MerchantRequestID);
//			 prep.setObject(2, CheckoutRequestID);
//			ResultSet rs=prep.executeQuery();
//			while(rs.next()) {
//				eslip_no=rs.getString("phone");
//			}
//			 conn.close();
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return eslip_no;
//	}
//	
//	public static String getEbillerRef(String MerchantRequestID,String CheckoutRequestID) {
//		String eslip_no="";
//		try {
//			String sql="SELECT ebiller_ref from mpesa_payments  WHERE MerchantRequestID=? AND CheckoutRequestID=?";
//			 conn=DbManager.getConnection();
//			 PreparedStatement prep=conn.prepareStatement(sql);
//			 prep.setObject(1,MerchantRequestID);
//			 prep.setObject(2, CheckoutRequestID);
//			ResultSet rs=prep.executeQuery();
//			while(rs.next()) {
//				eslip_no=rs.getString("ebiller_ref");
//			}
//			 conn.close();
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		return eslip_no;
//	}
	
//	public static void SendOTPSMS(String phone,String code,String message) {
//		try {
//		Hashtable<String, String> configsleg = new Hashtable<String, String>();
//		 String sms=message;
//        configsleg.put("msisdn", phone);
//        configsleg.put("message", sms);
//        configsleg.put("sms_id", code);
//        
//        // send and receive response from sms server
//        String resposnes = GeneralCodes.postRequest1(configsleg, ExternalFile.getSMSBaseUrl(), "JSON");
//        JSONObject obj = new JSONObject(resposnes);
//        String jsonResponse = obj.toString();
//        JsonParser parser = new JsonParser();
//        JsonElement jsonTree = parser.parse(jsonResponse);
//        com.google.gson.JsonObject jsonObject = jsonTree.getAsJsonObject();
//        JsonElement status = jsonObject.get("status");
//        JsonElement status_description = jsonObject.get("status_description");
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//       
//	}
	
	
	public static String getRandomText() {
		 int leftLimit = 97; // letter 'a'
		    int rightLimit = 122; // letter 'z'
		    int targetStringLength = 10;
		    Random random = new Random();
		 
		    String generatedString = random.ints(leftLimit, rightLimit + 1)
		      .limit(targetStringLength)
		      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
		      .toString();
		 
		   return generatedString.toUpperCase();
	}

}
