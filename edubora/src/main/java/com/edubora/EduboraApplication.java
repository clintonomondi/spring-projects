package com.edubora;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EduboraApplication {

	public static void main(String[] args) {
		SpringApplication.run(EduboraApplication.class, args);
	}

}
