package com.customer_socket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerSocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerSocketApplication.class, args);
	}

}
