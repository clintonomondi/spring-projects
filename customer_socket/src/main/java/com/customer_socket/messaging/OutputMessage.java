package com.customer_socket.messaging;


public class OutputMessage {
	 String from="";
     String text="";
     String time="";
     public OutputMessage(String from,String text, String time) {
    	 this.from = from;
    	 this.text = text;
    	 this.time=time;
     }
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
     
     
}
