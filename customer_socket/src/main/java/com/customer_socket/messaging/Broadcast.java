package com.customer_socket.messaging;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@EnableScheduling
@Controller
public class Broadcast {
//	@MessageMapping("/chat")
//	@SendTo("/topic/messages")
//	@Scheduled(fixedRate = 5000)
//	
//	
//	public static Object send() throws Exception {
//		System.out.println("Checking this service......");
//		  Thread.sleep(1000); // simulated delay
//	    String time = new SimpleDateFormat("HH:mm").format(new Date());
//	   Hashtable<String,Object>map=new Hashtable<String,Object>();
//	   map.put("message","succes");
//	   map.put("from","Test user");
//	   map.put("text","Am testing continously");
//	   map.put("time",time);
//	    return map;
//	   // return new OutputMessage(message.getFrom(), message.getText(), time);
//	}
	
	
	 @Autowired
	    private SimpMessagingTemplate template;

	    @Scheduled(fixedRate = 5000)
	    public void greeting() throws Exception {
	        Thread.sleep(500); // simulated delay
	        System.out.println("scheduled One running...");
	       
	     Object data=Query.numberOfSelfCreated();
	        this.template.convertAndSend("/topic/messages", data);
	    }
	
	    @Autowired
	    private SimpMessagingTemplate template2;

	    @Scheduled(fixedRate = 5000)
	    public void greeting2() throws Exception {
	        Thread.sleep(500); // simulated delay
	        System.out.println("scheduled Two running....");
	       
	       
				
				JSONObject req = new JSONObject();
				JSONObject json = new JSONObject();
				json.put("analytics", Query.onboarding_analytics());
				json.put("status", "00");
				json.put("statusMessage", "Success");
				
				
	        
	        this.template.convertAndSend("/topic/messages2", json);
	    }
	
}
