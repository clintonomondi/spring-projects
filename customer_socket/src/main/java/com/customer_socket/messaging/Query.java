package com.customer_socket.messaging;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.customer_socket.config.DbManager;

public class Query {
	static Connection conn;
	public static JSONObject numberOfSelfCreated() {

		try {

			//total backIdScanned
			String totalBackIdScanned = "0";
			conn = DbManager.getConnection();
			String sql = "SELECT COUNT(*) AS V_COUNT FROM `login_tracker` A WHERE REG_TYPE='CUSTOMER_SELF'";
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				totalBackIdScanned = rs.getString("V_COUNT");
			}
			conn.close();
			
			//total created accounts
			String totalSelfCreated = "0";
			conn = DbManager.getConnection();
			String sql1 = "SELECT COUNT(*) AS V_COUNT FROM `core_system_created_account` WHERE CUST_NO IN (SELECT CUST_NO FROM `login_tracker` A,`master_record` B WHERE REG_TYPE='CUSTOMER_SELF' AND A.ID_NO=B.ID_NO)";
			PreparedStatement prep1 = conn.prepareStatement(sql1);
			ResultSet rs1 = prep1.executeQuery();
			while (rs1.next()) {
				totalSelfCreated = rs1.getString("V_COUNT");
			}
			conn.close();

			// total scanned succssful  iprs/sms
			String noOfFetchedIprs = "0";
			conn = DbManager.getConnection();
			String sql2 = "SELECT COUNT(*) AS SCANNED_BACK FROM `login_tracker` A,`master_record` B WHERE REG_TYPE='CUSTOMER_SELF' AND A.ID_NO=B.ID_NO";
			PreparedStatement prep2 = conn.prepareStatement(sql2);
			ResultSet rs2 = prep2.executeQuery();
			while (rs2.next()) {
				noOfFetchedIprs = rs2.getString("SCANNED_BACK");
			}
			conn.close();

			// number of successful taken selfies
			String noOfSelfie = "0";
			conn = DbManager.getConnection();
			String sql3 = "SELECT COUNT(*) AS V_COUNT FROM `selfie_image_proc` WHERE CUST_NO IN (SELECT CUST_NO FROM `login_tracker` A,`master_record` B WHERE REG_TYPE='CUSTOMER_SELF' AND A.ID_NO=B.ID_NO)";
			PreparedStatement prep3 = conn.prepareStatement(sql3);
			ResultSet rs3 = prep3.executeQuery();
			while (rs3.next()) {
				noOfSelfie = rs3.getString("V_COUNT");
			}
			conn.close();

			// number of successfully process completed KYC
			String completedProcess = "0";
			conn = DbManager.getConnection();
			String sql4 = "SELECT COUNT(*) AS V_COUNT FROM `login_tracker` A,`master_record` B WHERE REG_TYPE='CUSTOMER_SELF' AND A.ID_NO=B.ID_NO AND KYC_COMPLETE_YN='Y'";
			PreparedStatement prep4 = conn.prepareStatement(sql4);
			ResultSet rs4 = prep4.executeQuery();
			while (rs4.next()) {
				completedProcess = rs4.getString("V_COUNT");
			}
			conn.close();
			
	       // number of successfully process frontId
			String scannedFrontId = "0";
			conn = DbManager.getConnection();
			String sql5 = "SELECT COUNT(*) AS V_COUNT FROM `ocr_front_back_images` WHERE CUST_NO IN (SELECT CUST_NO FROM `login_tracker` A,`master_record` B WHERE REG_TYPE='CUSTOMER_SELF' AND A.ID_NO=B.ID_NO)";
			PreparedStatement prep5 = conn.prepareStatement(sql5);
			ResultSet rs5 = prep5.executeQuery();
			while (rs5.next()) {
				scannedFrontId = rs5.getString("V_COUNT");
			}
			conn.close();

			JSONObject object = null;
			object = new JSONObject();
			object.put("Back Id", totalBackIdScanned);
			object.put("iprs",noOfFetchedIprs );
			object.put("Front Id",scannedFrontId);
			object.put("Selfies", noOfSelfie);
			object.put("Complete KYC", completedProcess);
			object.put("Number accounts",totalSelfCreated );
			//object.put("status", "00");
			//object.put("statusMessage", "Success");
			return object;

		} catch (Exception e) {
			
			JSONObject object = null;
			object = new JSONObject();
			try {
				//object.put("status", "01");
				object.put("statusMessage", "Error");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
            e.printStackTrace();
			return object;
		}

	}
	
	
	
	public static JSONArray onboarding_analytics() {
		JSONArray jsonArray = new JSONArray();
		try {
			conn = DbManager.getConnection();
			String sql = "SELECT ID_NO,PHONE_NO,CREATED_DATE,DAYS_ELAPSE,FRONT_ID,SELFIE,KRA,PREFERENCE,OCCUPATION,SIGNATURE,NEXT_OF_KIN,\r\n" + 
					"NAME,EMAIL,ACCOUNT_TYPE,CUST_NO,REG_TYPE,BACK_ID_NAME FROM `onboarding_analytics` \r\n" + 
					"";
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs = prep.executeQuery();
			while (rs.next()) {
				Hashtable<String, Object> json = new Hashtable<String, Object>();
				json.put("id_number", rs.getString("ID_NO"));
				json.put("phone_no", rs.getString("PHONE_NO"));
				json.put("created_date", rs.getString("CREATED_DATE"));
				json.put("reg_type", rs.getString("REG_TYPE"));
				json.put("email", rs.getString("EMAIL"));
				json.put("account_type", rs.getString("ACCOUNT_TYPE"));
				json.put("cust_no", rs.getString("CUST_NO"));
				json.put("name", rs.getString("NAME"));
				json.put("lapse_time", rs.getString("DAYS_ELAPSE"));
				json.put("step_0", rs.getString("FRONT_ID"));
				json.put("step_2", rs.getString("SELFIE"));
				json.put("step_3", rs.getString("KRA"));
				json.put("step_4", rs.getString("PREFERENCE"));
				json.put("step_5", rs.getString("OCCUPATION"));
				json.put("step_6", rs.getString("SIGNATURE"));
				json.put("step_7", rs.getString("NEXT_OF_KIN"));
				json.put("id_path", rs.getString("BACK_ID_NAME"));
				jsonArray.add(FormJson.jsonFormat(json));
			}
			conn.close();
		} catch (Exception e) {
		e.printStackTrace();
		}
		return jsonArray;
	}
}
