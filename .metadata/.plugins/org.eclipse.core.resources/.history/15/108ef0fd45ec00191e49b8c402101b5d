package com.stanbic.backendUsers;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;


@SpringBootApplication
@RequestMapping("/api")
public class BankUsers {
Connection conn;
	
	@RequestMapping(value = "/v1/createBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> createBankUser(@RequestBody BankUsersReqModel jsondata) {
		String username = TokenManager.tokenIssuedCompCode(ExternalFile.getSecretKey(), jsondata.getToken());
		if(jsondata.getUsername().isEmpty() || jsondata.getEmail().isEmpty() || jsondata.getGroup_id().isEmpty()
				|| jsondata.getOtherName().isEmpty() || jsondata.getSurname().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
		}
		if (username.equalsIgnoreCase("03")) {
			return ResponseEntity.ok(new AuthProduceJson("03", "Page has expired", "", ""));
		}

		if (username.equalsIgnoreCase("01")) {
			return ResponseEntity.ok(new AuthProduceJson("01", "An error occured,page might expired", "", ""));
		}
		int vcount=BankUserHelper.checkBankUser(jsondata.getUsername());
		if(vcount>0) {
			return ResponseEntity.ok(new BankUsersResp("08", "The user already in the local system", ""));
		}else {
       try {
			conn = DbManager.getConnection();
			String sql="INSERT INTO bank_users(USERNAME,EMAIL,SURNAME,OTHER_NAMES,group_id,PHONE,CREATED_BY) VALUES(?,?,?,?,?,?,?)";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getUsername().toUpperCase());
			prep.setObject(2, jsondata.getEmail().toUpperCase());
			prep.setObject(3, jsondata.getSurname().toUpperCase());
			prep.setObject(4, jsondata.getOtherName().toUpperCase());
			prep.setObject(5, jsondata.getGroup_id());
			prep.setObject(6, jsondata.getPhone());
			prep.setObject(7, username);
			prep.execute();
			conn.close();
			return ResponseEntity.ok(new BankUsersResp("00", "bank user successfully created", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "System technical error", ""));
		}
		}
	}
	
	@RequestMapping(value = "/v1/getBankUsers", method = RequestMethod.POST)
	public ResponseEntity<?> getBankUser(@RequestBody BankGetUsersReqModel jsondata) {
	String compCode=companyCode();
	
	  List<BankUsersRespModel> model=new ArrayList<BankUsersRespModel>();
       try {
			conn = DbManager.getConnection();
			String sql="SELECT id,username,other_names,phone,email,group_id,FROZEN_YN,\r\n" + 
					"(SELECT NAME FROM bank_groups B WHERE B.id=A.group_id)GROUP_NAME\r\n" + 
					" FROM bank_users A";
			PreparedStatement prep = conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				BankUsersRespModel m=new BankUsersRespModel();
				m.email=rs.getString("email");
				m.otherName=rs.getString("other_names");
				m.phone=rs.getString("phone");
				m.userGroup=rs.getString("GROUP_NAME");
				m.username=rs.getString("USERNAME");
				m.group_id=rs.getString("group_id");
				m.status=rs.getString("FROZEN_YN");
				model.add(m);
			}
			conn.close();
			return ResponseEntity.ok(model);
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(model);
		}
	}
	
	
	
	@RequestMapping(value = "/v1/editBankUsers", method = RequestMethod.POST)
	public ResponseEntity<?> editBankUser(@RequestBody BankEditUsersReqModel jsondata) {
    String system_username=TokenManager.tokenIssuedCompCode(ExternalFile.getSecretKey(), jsondata.getToken());
    if (system_username.equalsIgnoreCase("03")) {
		return ResponseEntity.ok(new AuthProduceJson("01", "Page has expired", "", ""));
	}
	if (system_username.equalsIgnoreCase("01")) {
		return ResponseEntity.ok(new AuthProduceJson("03", "An error occured,page might expired", "", ""));
	}
	if(jsondata.getUsername().isEmpty() || jsondata.getGroup_id().isEmpty()) {
		return ResponseEntity.ok(new AuthProduceJson("06", "Please provide all fields", "", ""));
	}
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET email=?,username=?,other_names=?,group_id=?,created_by=? WHERE username=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, jsondata.getEmail().toUpperCase());
			prep.setObject(2, jsondata.getUsername().toUpperCase());
			prep.setObject(3, jsondata.getOtherName().toUpperCase());
			prep.setObject(4, jsondata.getGroup_id());
			prep.setObject(5, system_username);
			prep.setObject(6, jsondata.getUsername());
			prep.executeUpdate();
			conn.close();
			return ResponseEntity.ok(new BankUsersResp("00", "Success", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/freezeBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> freezeBankUser(@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
		
    String system_username=TokenManager.tokenIssuedCompCode(ExternalFile.getSecretKey(), jsondata.getToken());
    if (system_username.equalsIgnoreCase("03")) {
		return ResponseEntity.ok(new AuthProduceJson("01", "Page has expired", "", ""));
	}
	if (system_username.equalsIgnoreCase("01")) {
		return ResponseEntity.ok(new AuthProduceJson("03", "An error occured,page might expired", "", ""));
	}
	if(system_username.equalsIgnoreCase(jsondata.getUsername())) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to deactivate this user", "", ""));
    }
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET FROZEN_YN=?,CREATED_BY=? WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "Y");
			prep.setObject(2, system_username);
			prep.setObject(3, jsondata.getUsername());
			prep.executeUpdate();
			conn.close();
			return ResponseEntity.ok(new BankUsersResp("00", "User dectivated successfully", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	@RequestMapping(value = "/v1/restoreBankUser", method = RequestMethod.POST)
	public ResponseEntity<?> restoreBankUser(@RequestBody BankEditUsersReqModel jsondata) {
		if(jsondata.getUsername().isEmpty()) {
			return ResponseEntity.ok(new AuthProduceJson("06", "Please provide username", "", ""));
		}
    String system_username=TokenManager.tokenIssuedCompCode(ExternalFile.getSecretKey(), jsondata.getToken());
    if (system_username.equalsIgnoreCase("03")) {
		return ResponseEntity.ok(new AuthProduceJson("01", "Page has expired", "", ""));
	}
	if (system_username.equalsIgnoreCase("01")) {
		return ResponseEntity.ok(new AuthProduceJson("03", "An error occured,page might expired", "", ""));
	}
    if(system_username.equalsIgnoreCase(jsondata.getUsername())) {
    	return ResponseEntity.ok(new AuthProduceJson("08", "You are not authorized to activate this user", "", ""));
    }
       try {
			conn = DbManager.getConnection();
			String sql="UPDATE bank_users SET FROZEN_YN=?,CREATED_BY=? WHERE USERNAME=?";
			PreparedStatement prep = conn.prepareStatement(sql);
			prep.setObject(1, "N");
			prep.setObject(2, system_username);
			prep.setObject(3, jsondata.getUsername());
			prep.executeUpdate();
			conn.close();
			return ResponseEntity.ok(new BankUsersResp("00", "User restored successfully", ""));
		}catch(Exception e) {
			e.printStackTrace();
			return ResponseEntity.ok(new BankUsersResp("02", "Error", ""));
		}
	}
	
	public int checkIfGroupExist(String companyCode,String groupId) {
		int v_count=0;
		try {
			String sql="SELECT COUNT(*) AS V_COUNT FROM `user_groups` WHERE comp_code=? AND id=?";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			prep.setObject(1, companyCode);
			prep.setObject(2, groupId);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				
			}
			return v_count;
		}catch(Exception e) {
			return v_count;
		}
	}
	
	public String companyCode() {
		String bankCode="";
		try {
			String sql="SELECT BANK_CODE FROM `general_settings` WHERE ID=1";
			conn=DbManager.getConnection();
			PreparedStatement prep=conn.prepareStatement(sql);
			ResultSet rs=prep.executeQuery();
			while(rs.next()) {
				bankCode=rs.getString("BANK_CODE");
			}
			conn.close();
			return bankCode;
		}catch(Exception e) {
			return bankCode;
		}
	}
	
	
	
}
