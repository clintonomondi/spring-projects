package com.stanbic.Policy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stanbic.Accounts.AccountHelper;
import com.stanbic.FileMapping.CheckOff_Mapping_Model;
import com.stanbic.KPLC.QueryBulkBillsService;
import com.stanbic.Responses.Response;
import com.stanbic.comm.DbManager;
import com.stanbic.comm.ExternalFile;
import com.stanbic.comm.FileManager;
import com.stanbic.comm.TokenManager;
import com.stanbic.ebiller.auth.AuthProduceJson;

@SpringBootApplication
@RequestMapping("/api")
public class PayerPolicy {

	
	Connection conn=null;
	
	 @RequestMapping(value = "/v1/payerUploadCheckOffPolicy", method = RequestMethod.POST)
	 public ResponseEntity<?> payerUploadCheckOffPolicy(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CheckOff_Mapping_Model jsondata) {
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		 if(jsondata.getBiller_code().isEmpty() || jsondata.getBase64Excel().isEmpty() || jsondata.getDate().isEmpty()) {
				return ResponseEntity.ok(new Response("06", "Please  upload excel file and select date for policy"));
			}
			if(PolicyHelper.checkPolicyPayerSettings(payer_code,jsondata.getBiller_code()) !=1) {
				 return ResponseEntity.ok(new AuthProduceJson("06","Please make policy file mapping with this biller before uploading the policies","",""));
			 }
			try {
			 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
				LocalDateTime now = LocalDateTime.now();
				String time = dtf.format(now); 
				String filename = time.replace("/", "_") + jsondata.getBiller_code();
				String path = ExternalFile.getExcelPath()+AccountHelper.generateRandomInt(4) + filename + ".xlsx";
				FileManager.saveImage(jsondata.getBase64Excel(), path);
				 Vector data=QueryBulkBillsService.read(path);
				 String file_id = AccountHelper.generateRandomInt(99999) + time;
				 List list=PolicyHelper.CheckUmbrellaAmount(data, jsondata.getBiller_code(), payer_code, file_id);
				 
				 HashMap<String, Object> map2 = new HashMap<String, Object>();
					map2.put("messageCode", "00");
					map2.put("message", "The following are the policies confirmations");
					map2.put("policies", list);
					map2.put("file_id", file_id);
					map2.put("biller_code", jsondata.getBiller_code());
					map2.put("path",path);
					map2.put("date",jsondata.getDate());
					return ResponseEntity.ok().body(map2);
			}catch(Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new Response("02", "System technical error"));
			}
				 	 
	 }
	 
	 @RequestMapping(value = "/v1/payerProceedPolicy", method = RequestMethod.POST)
	 public ResponseEntity<?> payerProceedPolicy(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CheckOff_Mapping_Model jsondata) {
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		 if(jsondata.getPath().isEmpty() || jsondata.getFile_id().isEmpty() || jsondata.getBiller_code().isEmpty() || jsondata.getDate().isEmpty()) {
			 return ResponseEntity.ok(new Response("06", "Submit biller code, file id, path and date"));
		 }
		 
			try {
				Vector data=QueryBulkBillsService.read(jsondata.getPath());
				Thread t = new Thread() {
					public void run() {
						PolicyHelper.saveCheckOffPolcies(data, jsondata.getBiller_code(), payer_code, jsondata.getFile_id(),jsondata.getDate());
					}
				};
				t.start(); 
		 
		 return ResponseEntity.ok(new Response("00", "Policies uploaded  successfully"));
				
			}catch(Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new Response("02", "System technical error"));
			}
				 	 
	 }
	 
	 @RequestMapping(value = "/v1/payerUploadUmbrellaPolicy", method = RequestMethod.POST)
	 public ResponseEntity<?> payerUploadUmbrellaPolicy(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CheckOff_Mapping_Model jsondata) {
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		 if(jsondata.getBiller_code().isEmpty() || jsondata.getBase64Excel().isEmpty() || jsondata.getDate().isEmpty()) {
				return ResponseEntity.ok(new Response("06", "Please select a payillerer and upload excel file and select data for policy"));
			}
		 if(PolicyHelper.checkPolicyPayerSettings(payer_code,jsondata.getBiller_code()) !=1) {
			 return ResponseEntity.ok(new AuthProduceJson("06","Please make policy file mapping with this biller before uploading the policies","",""));
		 }
			try {
			 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
				LocalDateTime now = LocalDateTime.now();
				String time = dtf.format(now); 
				String filename = time.replace("/", "_") + jsondata.getBiller_code();
				String path = ExternalFile.getExcelPath()+AccountHelper.generateRandomInt(4) + filename + ".xlsx";
				FileManager.saveImage(jsondata.getBase64Excel(), path);
				 Vector data=QueryBulkBillsService.read(path);
				 String file_id = AccountHelper.generateRandomInt(99999) + time;
				 
				 Thread t = new Thread() {
						public void run() {
							PolicyHelper.saveCheckOffPolcies(data, jsondata.getBiller_code(), payer_code, file_id,jsondata.getDate());
						}
					};
					t.start(); 
			 
			 return ResponseEntity.ok(new Response("00", "Policies uploaded  successfully"));
				
				 
			}catch(Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new Response("02", "System technical error"));
			}
				 	 
	 }
	 
	 @RequestMapping(value = "/v1/payerGetMyPolicies", method = RequestMethod.POST)
	 public ResponseEntity<?> payerMyGetPolicies(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CheckOff_Mapping_Model jsondata) {
		 List<PolicyModel> model= new ArrayList<PolicyModel>();
		 String payer_code=TokenManager.tokenIssuedCompCode(token);
		 if(jsondata.getBiller_code().isEmpty()) {
			 return ResponseEntity.ok(new Response("06", "We could not indetify the biller"));
		 }
		 
			try {
				 String sql="SELECT *,\r\n" + 
				 		"(SELECT SUM(amount) FROM policy_data B WHERE B.file_id=A.file_id)amount,\r\n" + 
				 		"(SELECT COUNT(*) FROM  policy_data B WHERE B.file_id=A.file_id)policies\r\n" + 
				 		" FROM `policy_files` A WHERE biller_code=? AND payer_code=?";
				 conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getBiller_code());
				 prep.setObject(2, payer_code);
				 ResultSet rs=prep.executeQuery();
				 while(rs.next()) {
					 PolicyModel data=new PolicyModel();
					 data.file_id=rs.getString("file_id");
					 data.date=rs.getString("date");
					 data.created_at=rs.getString("created_at");
					 data.status=rs.getString("status");
					 data.amount=rs.getString("amount");
					 data.policies=rs.getString("policies");
					 model.add(data);
				 }
				 conn.close();
		 return ResponseEntity.ok(model);
				
			}catch(Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new Response("02", "System technical error"));
			}
				 	 
	 }
	 
	 @RequestMapping(value = "/v1/getPolicyFileData", method = RequestMethod.POST)
	 public ResponseEntity<?> getPolicyFileData(HttpServletRequest request, @RequestHeader(name = "Authorization") String token,@RequestBody CheckOff_Mapping_Model jsondata) {
		 List<PolicyModel> model= new ArrayList<PolicyModel>();
		 if(jsondata.getFile_id().isEmpty()) {
			 return ResponseEntity.ok(new Response("06", "Provide file id"));
		 }
		 
			try {
				 String sql="SELECT *,\r\n" + 
				 		"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.payer_code)payer_name,\r\n" + 
				 		"(SELECT company_name FROM biller_profile B WHERE B.comp_code=A.biller_code)biller_name,\r\n" + 
				 		"(SELECT amount FROM checkoff_biller_policy B WHERE B.payer_code=A.payer_code AND B.biller_code=A.biller_code AND B.policy_no=A.policy_no)amount_on_biller,\r\n" + 
						"(SELECT policy_no FROM checkoff_biller_policy B WHERE B.payer_code=A.payer_code AND B.biller_code=A.biller_code AND B.policy_no=A.policy_no)policy_on_biller\r\n" + 
				 		" FROM policy_data A WHERE file_id=?";
				 conn=DbManager.getConnection();
				 PreparedStatement prep=conn.prepareStatement(sql);
				 prep.setObject(1, jsondata.getFile_id());
				 ResultSet rs=prep.executeQuery();
				 while(rs.next()) {
					 PolicyModel data=new PolicyModel();
					 data.file_id=rs.getString("file_id");
					 data.date=rs.getString("date");
					 data.created_at=rs.getString("created_at");
					 data.status=rs.getString("status");
					 data.amount=rs.getString("amount");
					 data.payer_name=rs.getString("payer_name");
					 data.biller_name=rs.getString("biller_name");
					 data.policy_no=rs.getString("policy_no");
					 data.name=rs.getString("name");
					 
					 if(rs.getString("amount").equalsIgnoreCase(rs.getString("amount_on_biller"))) {
						 data.amount_status="EQUAL";
					 }else {
						 data.amount_status="NOT EQUAL";
					 }
					 
					 if(rs.getString("policy_no").equalsIgnoreCase(rs.getString("policy_on_biller"))) {
						 data.policy_holder_status="FOUND";
					 }else {
						 data.policy_holder_status="NOT FOUND";
					 }
					 model.add(data);
				 }
				 conn.close();
		 return ResponseEntity.ok(model);
				
			}catch(Exception e) {
				e.printStackTrace();
				return ResponseEntity.ok(new Response("02", "System technical error"));
			}
				 	 
	 }
}
