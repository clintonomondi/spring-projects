package com.writerapi.writerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WriterapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WriterapiApplication.class, args);
	}

}
