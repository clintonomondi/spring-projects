package com.writerapi.writerapi.RabbitMQ;

import org.apache.velocity.VelocityContext;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.writerapi.writerapi.common.EmailManager;

@Service
public class EmailAutoConsume {

	@RabbitListener(queues = "DevMyessay_Email_Queue")
	public void getString(String jsondata) {
	    System.out.println("Message from Queues=>>>>"+jsondata);
	    try {
	    	JSONObject obj = new JSONObject(jsondata);
	    	String jsonResponse = obj.toString();
	    	Gson gson = new GsonBuilder().create();
	    	EmailModel res = gson.fromJson(jsonResponse, EmailModel.class);
	    	String email = res.getEmail();
	    	String subject = res.getSubject();
	    	String message = res.getMessage();
	    	
	    	Thread t = new Thread() {
				public void run() {
			 VelocityContext vc = new VelocityContext();
	          vc.put("message", message);
	          vc.put("subject", subject);
	          String email_template = EmailManager.email_message_template(vc,"email.vm");
				EmailManager.send_mail(email,subject,email_template);
		
		}};
		t.start(); 
	    	
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	      
	}
}
