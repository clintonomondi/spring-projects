package com.writerapi.writerapi.RabbitMQ;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.writerapi.writerapi.common.ExternalFile;


@Configuration
public class BrokerConfig {

	@Bean
	public ConnectionFactory connectionFactory() {
		try {
		CachingConnectionFactory connection = new CachingConnectionFactory();
		CachingConnectionFactory connectionFactory = new
				CachingConnectionFactory(ExternalFile.Rabbit_Host());
				connectionFactory.setUsername(ExternalFile.Rabbit_Username());
				connectionFactory.setPassword(ExternalFile.Rabbit_Password());
				connectionFactory.setPort(Integer.parseInt(ExternalFile.Rabbit_Port()));
				return connectionFactory;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
