package com.writerapi.writerapi.mail;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.apache.velocity.VelocityContext;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.writerapi.writerapi.common.EmailManager;
import com.writerapi.writerapi.common.ExternalFile;




@SpringBootApplication
@RequestMapping("/api")
public class email {
	 ConnectionFactory factory = null; 
	 com.rabbitmq.client.Connection connection = null; 
	 Channel channel = null; 
	
	@RequestMapping(value = "/v1/email", method = RequestMethod.POST)
	 public ResponseEntity<?> email(@RequestBody mailModel jsondata){
		
//		 Thread t = new Thread() {
//				public void run() {
//		try {
//		
//
//			 VelocityContext vc = new VelocityContext();
//	          vc.put("message", jsondata.getMessage());
//	          vc.put("subject", jsondata.getSubject());
//	          String email_template = EmailManager.email_message_template(vc,"email.vm");
//				EmailManager.send_mail(jsondata.getEmail(),jsondata.getSubject(),email_template);
//			
//		}catch(Exception e) {
//			e.printStackTrace();
//		}
//		
//		}};
//		t.start(); 
//		return null;
		
		try { 
			String mq_username = ExternalFile.Rabbit_Username(); 
			String mq_password =ExternalFile.Rabbit_Password(); 
			String mq_port=ExternalFile.Rabbit_Port(); 
			String mq_hostname = ExternalFile.Rabbit_Host(); 
			boolean durable = true; 
			factory = new ConnectionFactory(); 
			factory.setHost(mq_hostname); 
			factory.setPort(Integer.parseInt(mq_port)); 
			factory.setUsername(mq_username); 
			factory.setPassword(mq_password); 
			 
			connection = factory.newConnection(); 
			channel = connection.createChannel(); 
			
			JSONObject map = new JSONObject();
			map.put("email",jsondata.getEmail());
			map.put("subject",jsondata.getSubject());
			map.put("message",jsondata.getMessage());
			 
			channel.exchangeDeclare("DevMyessay_Email_Exchange", "direct", durable, false, null); 
			channel.basicPublish("DevMyessay_Email_Exchange", "DevMyessay_Email_Queue", null, map.toString().getBytes()); 
			
			channel.close(); 
			connection.close(); 
			return ResponseEntity.ok("Success");
			 
			} catch (Exception e) { 
				e.printStackTrace();
				return ResponseEntity.ok("Error");
			} 
			} 
	 }

